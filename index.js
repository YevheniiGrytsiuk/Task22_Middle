"use strict";

const button = document.getElementById("box-input-btn");

const tableDiv = document.getElementById('box-tbl-div');

button.addEventListener('click', function () {

    const table = document.getElementById("table-in-box");

    if (table != null) {
        tableDiv.removeChild(table);
    }

    const rows = document.getElementById("box-input-row").value;
    const cols = document.getElementById("box-input-col").value;

    let tbl = document.createElement('table');
    tbl.id = "table-in-box";
    tbl.style.width = '100%';

    let tbody = document.createElement('tbody');

    for (let i = 0; i < rows; i++) {

        let tr = document.createElement('tr');

        for (let j = 0; j < cols; j++) {

            let td = tr.insertCell();
            td.appendChild(document.createTextNode(`row: ${i} col: ${j}`));
            td.className = "simp";
            td.id = `${i}/${j}`;
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
    }

    tbl.appendChild(tbody);
    tableDiv.appendChild(tbl);
});

let selectedTd;

tableDiv.addEventListener('click', function (event) {

    let target = event.target;

    if (target.tagName != 'TD') 
        return;

    if (selectedTd) {
        if (target.className == 'highlight') {
            target.className = 'simp';
            selectedTd = null;
        }
        else if (target.className == "simp") {
            target.className = 'highlight';
            selectedTd.className = 'simp';
            selectedTd = target;    
        }
    }
    else {
        selectedTd = target;
        selectedTd.className = 'highlight';
    }

});